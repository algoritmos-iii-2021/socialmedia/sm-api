import {
  FilterableField,
  FilterableRelation,
} from '@nestjs-query/query-graphql';
import { ObjectType, Field, ID, GraphQLISODateTime } from '@nestjs/graphql';
import { User } from 'src/users/entities/user.entity';
/* import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'; */
@FilterableRelation('user', () => User)
/* @Entity() */
@ObjectType('Publication')
export class PublicationDTO {
  @Field(() => ID)
  @FilterableField()
  /*   @PrimaryGeneratedColumn() */
  id: number;

  /*   @Column() */
  @FilterableField()
  audio: string;

  /*   @ManyToOne(() => User, (user) => user.publications)
  user: User;
 */
  /*   @CreateDateColumn() */
  @Field(() => GraphQLISODateTime)
  createdAt: Date;

  /*   @UpdateDateColumn() */
  @Field(() => GraphQLISODateTime)
  updatedAt: Date;

  /*   @DeleteDateColumn() */
  @Field(() => GraphQLISODateTime, { nullable: true })
  DeletedAt: Date;
}
