import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreatePublicationInput {
  @Field()
  audio: string;
}
