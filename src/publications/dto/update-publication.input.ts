import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class UpdatePublicationInput {
  @Field({ nullable: true })
  id: number;
}
