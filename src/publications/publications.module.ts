import { Module } from '@nestjs/common';
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { Publication } from './entities/publication.entity';
import { User } from 'src/users/entities/user.entity';
import { CreatePublicationInput } from './dto/create-publication.input';
import { UpdatePublicationInput } from './dto/update-publication.input';
import { PublicationDTO } from './dto/publicationDTO.input';

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Publication, User])],

      resolvers: [
        {
          EntityClass: Publication,
          DTOClass: PublicationDTO,
          CreateDTOClass: CreatePublicationInput,
          UpdateDTOClass: UpdatePublicationInput,
        },
      ],
    }),
  ],
  providers: [],
})
export class PublicationsModule {}
